#!/bin/bash
# This script is used to,
#	delete default server log files
#	zip and save application specific log files

#file path for logs of this script 
log_path=/var/log/millionspaces/logs; 

if [ ! -d $log_path ]
then 
	mkdir -p $log_path;
	cd $log_path;
	touch deleteServerLogs.txt;
else
	cd $log_path;
	if [ ! -f deleteServerLogs.txt ]
	then 
		touch deleteServerLogs.txt;
	fi
fi

log_file_path=$log_path/deleteServerLogs.txt;


#Tomcat path, where server instances are located 
tomcat_path=/opt/Tomcat;
if [ ! -d $tomcat_path ]
then 
	echo "Tomcat directory cannot be found";
	echo "Tomcat directory cannot be found ">>$log_file_path;
	exit 0;
fi

#file path for eventspace default logs 
ziped_log_path=/var/log/millionspaces/logs; 
cd $ziped_log_path;

if [ ! -d savedLogs ]
then
	mkdir savedLogs;
fi

ziped_log_path_tom=$ziped_log_path/savedLogs;

for ((i=1; i<=4; i++))
do
	if [ ! -d $ziped_log_path_tom/tomcat$i  ]
	then
		mkdir $ziped_log_path_tom/tomcat$i;
	fi
done


#get the current date
date=$(date);
echo "---------------------------------------------------------">>$log_file_path;
echo "Logged date - $date">>$log_file_path;

#Get the yesterday
yesterday=$(date -d '-1 day' '+%Y-%m-%d');

#get the full file name of last day server.log
lastLogFile="server.log.$yesterday";

#files to be deleted
file1='catalina.2017-*';
file2='host-*';
file3='localhost*';
file4='manager*';

#get tomcat instances
cd $tomcat_path;
instances=$(ls -d tomcat* | wc -l);

#clear logs function
clearLogs(){

	cd $tomcat_path/tomcat$i/logs/;

	#remove files in file_path
	rm $file1
	rm $file2
	rm $file3
	rm $file4

	cd $tomcat_path/tomcat$i/logs/eventspace/;

	if [ -f $lastLogFile ]
	then 
		#backup the last log file as zip
		tar -zcvf $lastLogFile.tar.gz $lastLogFile;
		
		ziped_path=$ziped_log_path_tom/tomcat$i;
		
		#move file to the given location
		if [ $i == 1 ]
		then
			
			mv $lastLogFile.tar.gz $ziped_path/$lastLogFile.tar.gz;
			echo "$lastLogFile moved successfully ">>$log_file_path;
		fi
		
		if [ $i == 2 ]
		then
			mv $lastLogFile.tar.gz $ziped_path/$lastLogFile.tar.gz;
			echo "$lastLogFile moved successfully ">>$log_file_path;
		fi
		
		if [ $i == 3 ]
		then
			mv $lastLogFile.tar.gz $ziped_path/$lastLogFile.tar.gz;
			echo "$lastLogFile moved successfully ">>$log_file_path;
		fi
		
		if [ $i == 4 ]
		then
			mv $lastLogFile.tar.gz $ziped_path/$lastLogFile.tar.gz;
			echo "$lastLogFile moved successfully ">>$log_file_path;
		fi	

		#remove last log file
		rm $lastLogFile;
	else
		echo "$lastLogFile canot be found ">>$log_file_path;
	fi
}


for ((i=1; i<=$instances; i++))
do
	tom=(tomcat$i);

	if [ -d ${!tom} ]
	then
		clearLogs $i;
	else
		echo "${!tom} cannot be found";
	fi
done



#end
#ravi.jayasundara@auxenta.com
