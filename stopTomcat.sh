#!/bin/bash
#Tomcat replication script

#file path for logs of this script
log_path=/var/log/millionspaces/logs;
if [ ! -d $log_path ]
then 
	mkdir $log_path;
	cd $log_path;
	touch stopTomcat.txt;
else
	cd $log_path;
	if [ ! -f stopTomcat.txt ]
	then 
		touch stopTomcat.txt;
	fi
fi

log_file_path=$log_path/stopTomcat.txt;

#get the current date
date=$(date);
echo "----------------------------------------------------------------------">>$log_file_path;
echo "Logged date - $date">>$log_file_path;

#ec2 path, where new server instances are going to be created
tomcat_path=/opt/Tomcat/;
echo "tomcat_path - $tomcat_path">>$log_file_path;

#get how many instances user needs
read -a list -p "Which instance/instances do you need to stop [1 to 4]? ";

instances=${list[@]}; 

for((i=1; i<=instances; i++))
do
	cd $tomcat_path/tomcat$i/bin;
	sudo bash shutdown.sh;
	echo "Tomcat instance $i is going to be shutting down">>$log_file_path;
	
done


#end
#ravi.jayasundara@auxenta.com

	

