#!/bin/bash
#Tomcat replication script

#file path for logs of this script
log_path=/var/log/millionspaces/logs; 
if [ ! -d $log_path ]
then 
	mkdir $log_path;
	cd $log_path;
	touch tomcatConfig.txt;
else
	cd $log_path;
	if [ ! -f tomcatConfig.txt ]
	then 
		touch tomcatConfig.txt;
	fi
fi

log_file_path=$log_path/tomcatConfig.txt;

#get the current date
date=$(date);
echo "----------------------------------------------------------------------">>$log_file_path;
echo "Logged date - $date">>$log_file_path;

#default tomcat path, where original server instance is located.
default_tomcat=/opt/config/tomcat/;
echo "default_tomcat - $default_tomcat">>$log_file_path;

#ec2 path, where new server instances are going to be created
tomcat_path=/opt/Tomcat/;
echo "tomcat_path - $tomcat_path">>$log_file_path;


#config file location, where server config files are located
config_file=/opt/config/configFiles/;

#get how many instances user needs
read -p "How many instances do you want [1 to 4]? " instances;

#create new tomcat instances
for ((i=1; i<=$instances; i++))
do
	#copy tomcat instance
	if [ -d $default_tomcat ]
	then
		#copy tomcat 
		cp -R $default_tomcat $tomcat_path;
		#rename tomcat
		mv $tomcat_path/tomcat $tomcat_path/tomcat$i;
	else
		echo "Default tomcat is not available in $default_tomcat">>$log_file_path;
	fi

	#copy config file
	if [ -s $config_file/server$i.xml ] 
	then
		cp $config_file/server$i.xml $tomcat_path/tomcat$i/conf/server.xml;
	else
		echo "Server config is not available in $server1">>$log_file_path;
	fi
done


#end
#ravi.jayasundara@auxenta.com

