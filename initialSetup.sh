#!/bin/bash


#file path for logs of this script 
log_file_path=/var/log/millionspaces/logs/initSetup.txt;


cd /home;
sudo wget https://s3-ap-southeast-1.amazonaws.com/ms-configurations/tomcat.tar.gz;


if [ -f tomcat.tar.gz ]
then
	echo "Tomcat instance has been downloaded ";
	echo "Tomcat instance has been downloaded ">>$log_file_path;
	sudo mv tomcat.tar.gz /opt/config/;
	cd /opt/config;
	tar -xzvf tomcat.tar.gz tomcat;

	if [ ! -d tomcat ]
	then
		echo "Tomcat instance has been unziped in /opt/config location ";
		echo "Tomcat instance has been unziped in /opt/config location ">>$log_file_path;
	fi
fi

cd /home;
sudo wget https://s3-ap-southeast-1.amazonaws.com/ms-configurations/tomcatConfigFiles.tar.gz;

if [ -f tomcatConfigFiles.tar.gz ]
then
	echo "Tomcat instance has been downloaded ";
	echo "Tomcat instance has been downloaded ">>$log_file_path;
	sudo mv tomcatConfigFiles.tar.gz /opt/config/;
	cd /opt/config;
	tar -xzvf tomcatConfigFiles.tar.gz configFiles;
	
	if [ ! -d tomcatConfigFiles ]
	then
		echo "Tomcat instance has been unziped in /opt/config location ";
		echo "Tomcat instance has been unziped in /opt/config location ">>$log_file_path;
	fi
fi

cd /opt/config/;
sudo rm tomcat.tar.gz && sudo rm tomcatConfigFiles.tar.gz;

#end
#ravi.jayasundara@auxenta.com
