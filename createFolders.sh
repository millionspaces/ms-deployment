#!/bin/bash

#file path for logs of this script 
log_path=/var/log/millionspaces/logs;

if [ ! -d $log_path ]
then 
	cd /var/log/;
	mkdir millionspaces;
	cd millionspaces;
	mkdir logs;
	cd /var/log/millionspaces/logs/;
	mkdir savedFiles;
	echo 'Log files locations have been created ';
else
	cd $log_path;
	
	if [ ! -d savedFiles ]
	then 
		mkdir savedFiles;
	fi
fi


cd /opt;

if [ ! -d Tomcat ]	
then
	mkdir Tomcat;
	echo 'Main Tomcat directory has been created ';
fi

if [ ! -d config ]
then
	mkdir config;
	echo "config directory has been created ";
fi
