#!/bin/bash
#This script is used to install Java

#install openjdk-8-jre
sudo apt-get install openjdk-8-jre;

#install openjdk-8-jdk
sudo apt-get install openjdk-8-jdk;

#install oracle java
sudo add-apt-repository ppa:webupd8team/java;
sudo apt-get update;
sudo apt-get install oracle-java8-installer;

echo "Please check your Java version by, java -version";
#end
#ravi.jayasundara@auxenta.com



