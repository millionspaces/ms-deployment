#!/bin/bash
#start tomcat instances

#file path for logs of this script
log_path=/var/log/millionspaces/logs; 
if [ ! -d $log_path ]
then 
	mkdir $log_path;
	cd $log_path;
	touch tomcatStartLogs.txt;
else
	cd $log_path;
	if [ ! -f tomcatStartLogs.txt ]
	then 
		touch tomcatStartLogs.txt;
	fi
fi

log_file_path=$log_path/tomcatStartLogs.txt;

#get the current date
date=$(date);
echo "----------------------------------------------------------------------">>$log_file_path;
echo "Logged date - $date">>$log_file_path;

#This is where new server instances are going to be created
tomcat_path=/opt/Tomcat;

if [ ! -d $tomcat_path ]
then 
	echo "EC2 instance path cannot be found";
	echo "EC2 instance path cannot be found ">>$log_file_path;
	exit 0;
fi


#check number of instances
cd $tomcat_path;

if [ -d tomcat* ]
then
	instances=$(ls -d tomcat* | wc -l);
	echo "Number of tomcat instances - $instances">>$log_file_path;
	
	if [ $instances -ge 0 ]
	then
		for ((i=1; i<=$instances; i++))
		do
			#start tomcat
			bash $tomcat_path/tomcat$i/bin/startup.sh;
			echo "Tomcat instance $i started !!!">>$log_file_path;
			tail -f $tomcat_path/tomcat$i/log/catalina.out;
		done
	fi
else
	echo "Cannot find any tomcat insances">>$log_file_path;
fi


#end
#ravi.jayasundara@auxenta.com

