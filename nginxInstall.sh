#!/bin/bash


#file path for logs of this script 
log_file_path=/var/log/millionspaces/logs/initSetup.txt;

cd /etc/;
sudo rm -rf nginx;

cd /home;

#Download nginx configurations for millionspaces from s3 bucket
sudo wget https://s3-ap-southeast-1.amazonaws.com/ms-configurations/nginx.tar.gz


if [ -f nginx.tar.gz ]
then
	echo "Nginx has been downloaded from s3";
	echo "Nginx has been downloaded from s3">>$log_file_path;
	sudo mv nginx.tar.gz /etc;
	cd /etc;
	tar -xzvf nginx.tar.gz nginx;

	if [ ! -d nginx ]
	then
		echo "Nginx has been unziped in /opt/config location ";
		echo "Nginx has been unziped in /opt/config location ">>$log_file_path;
	fi
fi



#end
#ravi.jayasundara@auxenta.com
