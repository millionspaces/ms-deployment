#!/bin/bash


cd /home/;

if [ ! -d backend ]
then
	sudo mkdir backend;
fi

cd backend;

#Get the bitbucket username 
read -p "Please enter your username for bitbucket : " username;

#Clone project to /home/backend directory
sudo git clone https://$username@bitbucket.org/millionspaces/ms-spring-server.git;

echo "Project cloned successfully !";
