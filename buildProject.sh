#!/bin/bash

project_path=/home/backend/ms-spring-server;

cd $project_path;
sudo git pull;


sudo rm -rf $project_path/eventspace-web/target;
cd $project_path;

echo "You are going to create a normal build";
read -p "What is the type of the profile [qae,dev,beta,live,trg] : " profileType;

sudo mvn clean install -P $profileType;

cd /home;

if [ ! -d project ]
then
	sudo mkdir project;
fi

cd project;
if [ ! -d normalBuild ]
then
	sudo mkdir normalBuild;
fi

cd $project_path/eventspace-web/target;
sudo unzip -d /home/project/normalBuild/eventspace eventspace.war;
echo "Please goto /home/project/normalBuild/ location to find the eventspace ";

